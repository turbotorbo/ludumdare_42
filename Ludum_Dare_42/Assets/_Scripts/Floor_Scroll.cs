﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor_Scroll : MonoBehaviour {

    Material mat;

    [SerializeField]
    float ScrollSpeed;

    private void Start()
    {
        MeshRenderer myMeshRend = GetComponent<MeshRenderer>();
        mat = myMeshRend.material;

        
    }
    void FixedUpdate()
    {
        Scroll();
    }

    void Scroll()
    {
        Vector2 offset = mat.mainTextureOffset;
        offset.x = offset.x + (Static_Holder.levelSpeed * Time.deltaTime);
        offset.y = 0;
        mat.mainTextureOffset = offset;
    }
}
