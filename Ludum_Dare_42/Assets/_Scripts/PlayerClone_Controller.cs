﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClone_Controller : MonoBehaviour {

	float moveSpeed, cooldownTimer, cooldown, driftForce;
    [SerializeField] GameObject bullet, playerClone, dust;

    bool hit, dead, generateRandomPos;
    Vector2 randomPos;

    [SerializeField] AudioSource myAudioSource;
    [SerializeField] AudioClip hurt, explosion;

    private void Start()
    {
        moveSpeed = Random.Range(0.5f, 3);
        float myScale = Random.Range(0.5f, 1.5f);
        transform.localScale = new Vector2(myScale, myScale);
        hit = false;
        dead = false;
        generateRandomPos = true;

        GameObject dust_clone;
        dust_clone = Instantiate(dust, transform.position, transform.rotation);

        cooldownTimer = Static_Holder.shootingCooldown;
        cooldown = 4f;

        driftForce = -1.5f;
    }

    private void Update()
    {
        Shoot();
        Split();
        Death();
    }
    void FixedUpdate ()
    {
        Movement();
	}

    void Movement()
    {
        transform.position = new Vector2(transform.position.x + (Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime),
                                         transform.position.y + (Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime));

        transform.Translate(Vector2.right * driftForce * Time.deltaTime);
    }

    void Shoot()
    {
        cooldownTimer += Time.deltaTime;
        Static_Holder.shootingCooldown = cooldownTimer;

        if (cooldownTimer > cooldown)
        {
            if (Input.GetButtonDown("Jump"))
            {
                GameObject bullet_clone;
                bullet_clone = Instantiate(bullet, transform.position, transform.rotation);
                cooldownTimer = 0;
                //Play Sound
            }
        }
    }

    void Death()
    {
        if(dead)
        {
            //play sound
            if (!myAudioSource.isPlaying)
            {
                myAudioSource.PlayOneShot(explosion);
            }
            GameObject dust_clone;
            dust_clone = Instantiate(dust, transform.position, transform.rotation);
            Static_Holder.brothers_killed++;
            Destroy(gameObject);
        }
    }

    void Split()
    {
        if (hit)
        {
            //instantiate clone in approximation of player

            while (generateRandomPos)
            {
                randomPos = new Vector2(Random.Range(transform.position.x + 3, transform.position.x - 3),
                                        Random.Range(transform.position.y + 3, transform.position.y - 3));

                if (Physics.CheckSphere(randomPos, 1))
                {
                    generateRandomPos = true;
                }
                else
                {
                    generateRandomPos = false;
                    GameObject player_clone;
                    player_clone = Instantiate(playerClone, randomPos, transform.rotation);
                    Static_Holder.brothers_existing++;
                    hit = false;
                }
            }




        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "enemyBullet")
        {
            if (!myAudioSource.isPlaying)
            {
                myAudioSource.PlayOneShot(hurt);
            }
            generateRandomPos = true;
            hit = true;
        }

        if (col.tag == "MeatGrinder")
        {
            dead = true;
        }
    }
}
