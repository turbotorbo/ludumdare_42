﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    float bulletSpeed = 15;

    void Update()
    {
        DestroyWhenOffscreen();
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector2.right * bulletSpeed * Time.deltaTime);
    }

    void DestroyWhenOffscreen()
    {
        if (transform.position.x > 16 || transform.position.x < -16)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag != "Enemy")
        {
            Destroy(gameObject);
        }
    }

}

