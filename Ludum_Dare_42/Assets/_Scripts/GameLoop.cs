﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLoop : MonoBehaviour {

    [SerializeField] Text Result_Time, Result_Brothers, Result_DeadBrothers;
    [SerializeField] GameObject GameOverScreen;

    [SerializeField] AudioSource myAudioSource;
    [SerializeField] AudioClip myAudioClip;

    bool playSoundOnce;

	void Start () {
        Static_Holder.gameOver = false;
        GameOverScreen.SetActive(false);
        Static_Holder.brothers_existing = 0;
        Static_Holder.brothers_killed = 0;
        Time.timeScale = 1;
        playSoundOnce = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey("escape"))
        {
            SceneManager.LoadScene(0);
        }

        if (Static_Holder.gameOver)
        {
            ShowGameOverScreen();
        }
	}

    void ShowGameOverScreen()
    {
        GameOverScreen.SetActive(true);
        Time.timeScale = 0;
        if (playSoundOnce)
        {
            myAudioSource.PlayOneShot(myAudioClip);
            playSoundOnce = false;
        }
        Result_Time.text = UI_Controller.elapsedTime.ToString("#.0") + " Seconds";
        Result_Brothers.text = Static_Holder.brothers_existing.ToString() + " Brothers";
        Result_DeadBrothers.text = Static_Holder.brothers_killed.ToString() + " are dead";

        if(Input.GetKey("r"))
        {
            SceneManager.LoadScene(1);
        }

        if (Input.GetKey("escape"))
        {
            SceneManager.LoadScene(0);
        }
    }
}
