﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Controller : MonoBehaviour {

    float start, ySpeed, xSpeed, step, yStart, yTarget, shootTimer, shootCooldown;
    Vector2 startPos, targetPos;
    bool hit, moveUp;

    [SerializeField] GameObject enemyBullet, enemyDust;
    [SerializeField] AudioSource myAudioSource;
    [SerializeField] AudioClip shoot, explosion;

    void Start ()
    {
        //movement stuff
        startPos = new Vector2(transform.position.x, transform.position.y);
        ySpeed = 2f;
        xSpeed = 1;
        moveUp = true;

        //shooting stuff
        shootTimer = 0f;
        shootCooldown = 0.5f;

        //dying stuff
        hit = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        EnemyShoot();
        EnemyDeath();
	}

    private void FixedUpdate()
    {
        EnemyMovement();
    }

    void EnemyMovement()
    {
        //Vertical movement
        if(transform.position.y >= startPos.y + 2)
        {
            moveUp = false;
        }
        if(transform.position.y <= startPos.y)
        {
            moveUp = true;
        }

        
        if(moveUp)
        {
            transform.Translate(Vector2.up * ySpeed * Time.deltaTime);
        }
        if(!moveUp)
        {
            transform.Translate(Vector2.up * -ySpeed * Time.deltaTime);
        }
        
        
        //Horizontal movement
        transform.Translate(Vector2.right * -xSpeed * Time.deltaTime);
    }

    void EnemyShoot()
    {
        shootTimer += Time.deltaTime;
        if(shootTimer >= shootCooldown)
        {
            /*
            if (!myAudioSource.isPlaying)
            {
                myAudioSource.PlayOneShot(shoot);
            }
            */
            GameObject enemyBullet_clone;
            enemyBullet_clone = Instantiate(enemyBullet, transform.position, enemyBullet.transform.rotation);
            shootTimer = 0;
        }
    }

    void EnemyDeath()
    {
        if(hit)
        {
            
            myAudioSource.PlayOneShot(explosion);
            
            GameObject dust_clone;
            dust_clone = Instantiate(enemyDust, transform.position, transform.rotation);
            //play sound
            Destroy(gameObject);
        }

        if(transform.position.x < -17)
        {
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D col) //Collider 2D my man!
    {
        if (col.tag == "myBullet")
        {
            hit = true;
        }

        if(col.tag == "Obstacle")
        {
            moveUp = !moveUp;
        }
    }
}
