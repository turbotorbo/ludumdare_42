﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu_Controller : MonoBehaviour {

    [SerializeField]

    private void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Submit"))
        {
            SceneManager.LoadScene(1);
        }
        if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }
}
