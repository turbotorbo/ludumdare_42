﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_Scroll : MonoBehaviour {

    [SerializeField] GameObject BG1, BG2, BG3, FG1, FG2;
    [SerializeField] float scrollSpeedBG1, scrollSpeedBG2, scrollSpeedBG3, scrollSpeedFG1, scrollSpeedFG2;

    Material BG1mat, BG2mat, BG3mat, FG1mat, FG2mat;

    private void Start()
    {
        MeshRenderer myMeshRendBG1 = BG1.GetComponent<MeshRenderer>();
        BG1mat = myMeshRendBG1.material;

        MeshRenderer myMeshRendBG2 = BG2.GetComponent<MeshRenderer>();
        BG2mat = myMeshRendBG2.material;

        MeshRenderer myMeshRendBG3 = BG3.GetComponent<MeshRenderer>();
        BG3mat = myMeshRendBG3.material;

        MeshRenderer myMeshRendFG1 = FG1.GetComponent<MeshRenderer>();
        FG1mat = myMeshRendFG1.material;

        MeshRenderer myMeshRendFG2 = FG2.GetComponent<MeshRenderer>();
        FG2mat = myMeshRendFG2.material;
    }

    void FixedUpdate () {
        Scroll();
	}

    void Scroll()
    {
        //BG1
            Vector2 BG1offset = BG1mat.mainTextureOffset;
            BG1offset.x = BG1offset.x + (scrollSpeedBG1 * Time.deltaTime);
            BG1offset.y = 0;
            BG1mat.mainTextureOffset = BG1offset;

        //BG2
        Vector2 BG2offset = BG2mat.mainTextureOffset;
        BG2offset.x = BG2offset.x + (scrollSpeedBG2 * Time.deltaTime);
        BG2offset.y = 0;
        BG2mat.mainTextureOffset = BG2offset;

        //BG3
        Vector2 BG3offset = BG3mat.mainTextureOffset;
        BG3offset.x = BG3offset.x + (scrollSpeedBG3 * Time.deltaTime);
        BG3offset.y = 0;
        BG3mat.mainTextureOffset = BG3offset;

        //FG1
        Vector2 FG1offset = FG1mat.mainTextureOffset;
        FG1offset.x = FG1offset.x + (scrollSpeedFG1 * Time.deltaTime);
        FG1offset.y = 0;
        FG1mat.mainTextureOffset = FG1offset;

        //FG1
        Vector2 FG2offset = FG2mat.mainTextureOffset;
        FG2offset.x = FG2offset.x + (scrollSpeedFG2 * Time.deltaTime);
        FG2offset.y = 0;
        FG2mat.mainTextureOffset = FG2offset;

    }
}
