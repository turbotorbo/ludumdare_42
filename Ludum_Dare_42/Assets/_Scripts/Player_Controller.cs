﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {

    [SerializeField] float moveSpeed, cooldown, driftForce;
    [SerializeField] GameObject bullet, playerClone;

    bool hit, generateRandomPos, dead;
    Vector2 randomPos;
    float cooldownTimer;

    [SerializeField] AudioSource myAudioSource;
    [SerializeField] AudioClip hurt, shoot, explosion;

	void Start ()
    {
        hit = false;
        dead = false;
	}
	
	void Update ()
    {
        Shooting();
        Death();
        Split();
	}

    private void FixedUpdate()
    {
        Player_Movement();
    }

    void Player_Movement()
    {
        transform.position = new Vector2(transform.position.x + (Input.GetAxis("Horizontal")*moveSpeed*Time.deltaTime),
                                         transform.position.y + (Input.GetAxis("Vertical")*moveSpeed*Time.deltaTime));

        transform.Translate(Vector2.right * driftForce * Time.deltaTime);
    }

    void Shooting()
    {
        
        cooldownTimer += Time.deltaTime;
        Static_Holder.shootingCooldown = cooldownTimer;

        if(cooldownTimer > cooldown)
        {
            if (Input.GetButtonDown("Jump"))
            {
                GameObject bullet_clone;
                bullet_clone = Instantiate(bullet, transform.position, transform.rotation);
                if(!myAudioSource.isPlaying)
                {
                    myAudioSource.PlayOneShot(shoot);
                }
                cooldownTimer = 0;
                //Play Sound
            }
        }
        
    }

    void Death()
    {
        if(dead)
        {
            if (!myAudioSource.isPlaying)
            {
                myAudioSource.PlayOneShot(explosion);
            }
            //play animation
            //play sound
            Destroy(gameObject);
        }
    }

    void Split()
    {
        if(hit)
        {
            while(generateRandomPos)
            {
                randomPos = new Vector2(Random.Range(transform.position.x + 3, transform.position.x - 3),
                                        Random.Range(transform.position.y + 3, transform.position.y - 3));

                if (Physics.CheckSphere(randomPos, 1))
                {
                    generateRandomPos = true;
                }
                else
                {
                    generateRandomPos = false;
                    GameObject player_clone;
                    player_clone = Instantiate(playerClone, randomPos, transform.rotation);
                    Static_Holder.brothers_existing++;
                    hit = false;
                }
            }   
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "enemyBullet")
        {
            generateRandomPos = true;
            if (!myAudioSource.isPlaying)
            {
                myAudioSource.PlayOneShot(hurt);
            }
            hit = true;
        }

        if(col.tag == "MeatGrinder")
        {
            dead = true;
            Static_Holder.gameOver = true;
        }
    }
}
