﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Generation : MonoBehaviour {

    [SerializeField] GameObject Obstacle, Enemy;
    float coolDown_Obstacles, coolDown_Enemies, timer_Obstacles, timer_Enemies, timer_GameStart;
    bool spawnObstacle, spawnEnemy, startGame;

	void Start ()
    {
        timer_GameStart = 0;
        startGame = false;
	}
	
	void Update ()
    {
        timer_GameStart += Time.deltaTime;
        if(timer_GameStart > 3)
        {
            startGame = true;
        }

        if(startGame)
        {
            ObstacleSpawn();
            EnemySpawn();
        }
        
	}

    void ObstacleSpawn()
    {
        timer_Obstacles += Time.deltaTime;

        if(timer_Obstacles > coolDown_Obstacles)
        {
            spawnObstacle = true;
            timer_Obstacles = 0f;
        }

        if (spawnObstacle)
        {
            Vector2 randomPos;
            randomPos = new Vector2(20, Random.Range(0, -7));
            GameObject Obstacle_clone;
            Obstacle_clone = Instantiate(Obstacle, randomPos, transform.rotation);
            coolDown_Obstacles = Random.Range(0.1f, 2f);
            spawnObstacle = false;
        }
    }

    void EnemySpawn()
    {
        timer_Enemies += Time.deltaTime;

        if (timer_Enemies > coolDown_Enemies)
        {
            spawnEnemy = true;
            timer_Enemies = 0f;
        }

        if (spawnEnemy)
        {
            Vector2 randomPos;
            randomPos = new Vector2(20, Random.Range(0, -7));
            GameObject Enemy_clone;
            Enemy_clone = Instantiate(Enemy, randomPos, transform.rotation);
            coolDown_Enemies = Random.Range(0.5f, 2f);
            spawnEnemy = false;
        }
    }
}
