﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForegroundSortingLayer : MonoBehaviour {

    void Start()
    {
        Renderer renderer = GetComponent<MeshRenderer>();
        renderer.sortingLayerName = "LayerName";
        renderer.sortingOrder = 30;
    }
}
