﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSortingLayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Renderer renderer = GetComponent<MeshRenderer>();
        renderer.sortingLayerName = "LayerName";
        renderer.sortingOrder = -30;
    }
	
	

}
