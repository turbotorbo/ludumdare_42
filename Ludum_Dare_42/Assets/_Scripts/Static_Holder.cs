﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static_Holder : MonoBehaviour {

    public static float levelSpeed = 0.1f;
    public static int cloneCount = 0;

    public static int brothers_existing = 0;
    public static int brothers_killed = 0;

    public static bool gameOver;

    public static float shootingCooldown;

    public static float result_Time;
}
