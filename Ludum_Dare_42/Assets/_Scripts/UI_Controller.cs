﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour {

    [SerializeField] Text Timer;
    [SerializeField] Image HUD_Icon_BG;
    public static float elapsedTime = 0;
    // Use this for initialization
    void Start ()
    {
        elapsedTime = 0f;
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        elapsedTime += Time.deltaTime;
        ShowTimer();
        SetCooldownGauge();
      
	}

    void ShowTimer()
    {
        Timer.text = elapsedTime.ToString("#.0") + " Seconds";
    }

    void SetCooldownGauge()
    {
        HUD_Icon_BG.fillAmount = Static_Holder.shootingCooldown / 4;
    }
    


}
