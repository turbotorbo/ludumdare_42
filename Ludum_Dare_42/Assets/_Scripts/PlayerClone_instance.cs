﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClone_instance : MonoBehaviour {

    [SerializeField] float moveSpeed;
    [SerializeField] GameObject bullet, playerClone;

    bool hit, generateRandomPos, dead;
    Vector2 randomPos;

    void Start()
    {
        hit = false;
        dead = false;
    }

    void Update()
    {

        Shooting();
        Death();
        Split();
    }

    private void FixedUpdate()
    {
        Player_Movement();
    }

    void Player_Movement()
    {
        transform.position = new Vector2(transform.position.x + (Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime),
                                         transform.position.y + (Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime));
    }

    void Shooting()
    {
        if (Input.GetButtonDown("Jump"))
        {
            GameObject bullet_clone;
            bullet_clone = Instantiate(bullet, transform.position, transform.rotation);
            //Play Sound
        }
    }

    void Death()
    {
        if (dead)
        {
            //play animation
            //play sound
            Destroy(gameObject);
        }
    }

    void Split()
    {
        if (hit)
        {
            //instantiate clone in approximation of player

            while (generateRandomPos)
            {
                randomPos = new Vector2(Random.Range(transform.position.x + 3, transform.position.x - 3),
                                        Random.Range(transform.position.y + 3, transform.position.y - 3));

                if (Physics.CheckSphere(randomPos, 1))
                {
                    generateRandomPos = true;
                }
                else
                {
                    generateRandomPos = false;
                    GameObject player_clone;
                    player_clone = Instantiate(playerClone, randomPos, transform.rotation);
                    hit = false;
                }
            }




        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "enemyBullet")
        {
            generateRandomPos = true;
            hit = true;
        }

        if (col.tag == "MeatGrinder")
        {
            dead = true;
        }
    }
}
