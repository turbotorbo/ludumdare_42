﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_Controller : MonoBehaviour {

    [SerializeField] float speed;

    private void Start()
    {
        float myScale = Random.Range(1,4);
        transform.localScale = new Vector2(myScale, myScale);
    }

    private void Update()
    {
        DestroyWhenOffscreen();
    }

    void FixedUpdate ()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
	}

    void DestroyWhenOffscreen()
    {
        if(transform.position.x < -17)
        {
            Destroy(gameObject);
        }
    }
}
