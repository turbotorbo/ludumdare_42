﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyRotation : MonoBehaviour {

    [SerializeField] float rotSpeed;

	void FixedUpdate ()
    {
        transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
	}
}
