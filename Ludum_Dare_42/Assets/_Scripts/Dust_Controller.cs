﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dust_Controller : MonoBehaviour {

    float timer = 0f;
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //transform.Translate(Vector2.right * -4 * Time.deltaTime);
        SelfDestruct();
    }

    void SelfDestruct()
    {
        timer += Time.deltaTime;
        if(timer > 2)
        {
            Destroy(gameObject);
        }
    }
}
